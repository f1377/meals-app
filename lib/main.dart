import 'package:flutter/material.dart';

import './dummy-data.dart';
import './models/meal.dart';
import './screens/categories_screen.dart';
import './screens/category_meals_screen.dart';
import './screens/filters_screen.dart';
import './screens/meal_detail_screen.dart';
import './screens/tabs_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _glutenFree = false;
  bool _lactoseFree = false;
  bool _vegan = false;
  bool _vegetarian = false;

  List<Meal> _availableMeals = DUMMY_MEALS;
  List<Meal> _favoritesMeals = [];

  void _setFilters(
    bool glutenFree,
    bool lactoseFree,
    bool vegan,
    bool vegetarian,
  ) {
    setState(() {
      _glutenFree = glutenFree;
      _lactoseFree = lactoseFree;
      _vegan = vegan;
      _vegetarian = vegetarian;

      _availableMeals = DUMMY_MEALS.where((meal) {
        if (_glutenFree && !meal.isGlutenFree) {
          return false;
        }
        if (_lactoseFree && !meal.isLactoseFree) {
          return false;
        }
        if (_vegan && !meal.isVegan) {
          return false;
        }
        if (_vegetarian && !meal.isVegetarian) {
          return false;
        }

        return true;
      }).toList();
    });
  }

  void _toggleFavorite(String mealId) {
    final int existingIndex =
        _favoritesMeals.indexWhere((meal) => meal.id == mealId);

    if (existingIndex >= 0) {
      // find a meal
      setState(() {
        _favoritesMeals.removeAt(existingIndex);
      });
    } else {
      // did not find meal
      setState(() {
        _favoritesMeals.add(
          DUMMY_MEALS.firstWhere((meal) => meal.id == mealId),
        );
      });
    }
  }

  bool _isMealFavorite(String id) {
    return _favoritesMeals.any((meal) => meal.id == id);
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData textTheme = ThemeData();

    return MaterialApp(
      title: 'Meals App',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        colorScheme: textTheme.colorScheme.copyWith(secondary: Colors.amber),
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
              bodyText1: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              bodyText2: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              headline6: TextStyle(
                fontSize: 18,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.bold,
              ),
            ),
      ),
      // home: CategoriesScreen(),
      // initialRoute: '/', // not needed, is set default, only for changes here,
      routes: {
        '/': (context) => TabsScreen(_favoritesMeals),
        CategoryMealsScreen.routeName: (context) => CategoryMealsScreen(
              _availableMeals,
            ),
        MealDetailScreen.routeName: (context) =>
            MealDetailScreen(_toggleFavorite, _isMealFavorite),
        FiltersScreen.routeName: (context) => FiltersScreen(
              glutenFree: _glutenFree,
              lactoseFree: _lactoseFree,
              vegan: _vegan,
              vegetarian: _vegetarian,
              saveFilters: _setFilters,
            ),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
        // if (settings.name == '/meal-detail') {
        //   return ...;
        // } else if (settings.name == '/something-else') {
        //   return ...;
        // }
        return MaterialPageRoute(
          builder: (context) => CategoriesScreen(),
        );
      },
      onUnknownRoute: (settings) {
        print('Unknow Page');
        return MaterialPageRoute(
          builder: (context) => CategoriesScreen(),
        );
      },
    );
  }
}
