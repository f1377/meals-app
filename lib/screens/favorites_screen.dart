import 'package:flutter/material.dart';

import '../models/meal.dart';
import '../widgets/meal_item.dart';

class FavoritesScreen extends StatelessWidget {
  final List<Meal> favoritesMeals;

  FavoritesScreen(this.favoritesMeals);

  @override
  Widget build(BuildContext context) {
    if (favoritesMeals.isEmpty) {
      return Center(
        child: Text('You have no favorites yet - start adding some!'),
      );
    }

    return ListView.builder(
      itemBuilder: (context, index) {
        final meal = favoritesMeals[index];

        return MealItem(
          id: meal.id,
          affordability: meal.affordability,
          duration: meal.duration,
          complexity: meal.complexity,
          imageUrl: meal.imageUrl,
          title: meal.title,
        );
      },
      itemCount: favoritesMeals.length,
    );
  }
}
