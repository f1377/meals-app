import 'package:flutter/material.dart';

import '../models/meal.dart';
import '../widgets/meal_item.dart';

class CategoryMealsScreen extends StatefulWidget {
  static const String routeName = '/category-meals';

  final List<Meal> availableMeals;

  CategoryMealsScreen(this.availableMeals);

  @override
  _CategoryMealsScreenState createState() => _CategoryMealsScreenState();
}

class _CategoryMealsScreenState extends State<CategoryMealsScreen> {
  String _categoryTitle = '';
  late List<Meal> _displayedMeals;
  bool _loadedInitData = false;

  @override
  void didChangeDependencies() {
    if (!_loadedInitData) {
      final routeArgs =
          ModalRoute.of(context)!.settings.arguments as Map<String, String>;
      final String categoryId = routeArgs['id'] ?? '';
      _categoryTitle = routeArgs['title'] ?? '';

      _displayedMeals = widget.availableMeals.where((meal) {
        return meal.categories.contains(categoryId);
      }).toList();

      _loadedInitData = true;
    }

    super.didChangeDependencies();
  }

  // void _removeMeal(String mealId) {
  //   setState(() {
  //     displayedMeals.removeWhere((meal) => meal.id == mealId);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    ListView createRecipes() {
      return ListView.builder(
        itemBuilder: (context, index) {
          final meal = _displayedMeals[index];

          return MealItem(
            id: meal.id,
            affordability: meal.affordability,
            duration: meal.duration,
            complexity: meal.complexity,
            imageUrl: meal.imageUrl,
            title: meal.title,
          );
        },
        itemCount: _displayedMeals.length,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(_categoryTitle),
      ),
      body: createRecipes(),
    );
  }
}
